'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">candiag-angular documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="dependencies.html" data-type="chapter-link">
                                <span class="icon ion-ios-list"></span>Dependencies
                            </a>
                        </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse" ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' : 'data-target="#xs-components-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' :
                                            'id="xs-components-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MessageLoopComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MessageLoopComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageNotFoundComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PageNotFoundComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UploadDefinitionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UploadDefinitionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/VisualCanComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">VisualCanComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' : 'data-target="#xs-directives-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' :
                                        'id="xs-directives-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' }>
                                        <li class="link">
                                            <a href="directives/DndDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">DndDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' : 'data-target="#xs-injectables-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' :
                                        'id="xs-injectables-links-module-AppModule-eb8e323f655181333c442bb3d2476bc0"' }>
                                        <li class="link">
                                            <a href="injectables/SignalService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>SignalService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UploadService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UploadService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/WebsocketService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>WebsocketService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AppPage.html" data-type="entity-link">AppPage</a>
                            </li>
                            <li class="link">
                                <a href="classes/CanData.html" data-type="entity-link">CanData</a>
                            </li>
                            <li class="link">
                                <a href="classes/CanRaw.html" data-type="entity-link">CanRaw</a>
                            </li>
                            <li class="link">
                                <a href="classes/IBus.html" data-type="entity-link">IBus</a>
                            </li>
                            <li class="link">
                                <a href="classes/IConsumer.html" data-type="entity-link">IConsumer</a>
                            </li>
                            <li class="link">
                                <a href="classes/IDocument.html" data-type="entity-link">IDocument</a>
                            </li>
                            <li class="link">
                                <a href="classes/ILabel.html" data-type="entity-link">ILabel</a>
                            </li>
                            <li class="link">
                                <a href="classes/ILabelGroup.html" data-type="entity-link">ILabelGroup</a>
                            </li>
                            <li class="link">
                                <a href="classes/ILabelSet.html" data-type="entity-link">ILabelSet</a>
                            </li>
                            <li class="link">
                                <a href="classes/IMessage.html" data-type="entity-link">IMessage</a>
                            </li>
                            <li class="link">
                                <a href="classes/IMultiplex.html" data-type="entity-link">IMultiplex</a>
                            </li>
                            <li class="link">
                                <a href="classes/IMuxGroup.html" data-type="entity-link">IMuxGroup</a>
                            </li>
                            <li class="link">
                                <a href="classes/INetworkDefinition.html" data-type="entity-link">INetworkDefinition</a>
                            </li>
                            <li class="link">
                                <a href="classes/INode.html" data-type="entity-link">INode</a>
                            </li>
                            <li class="link">
                                <a href="classes/INodeRef.html" data-type="entity-link">INodeRef</a>
                            </li>
                            <li class="link">
                                <a href="classes/IProducer.html" data-type="entity-link">IProducer</a>
                            </li>
                            <li class="link">
                                <a href="classes/ISignal.html" data-type="entity-link">ISignal</a>
                            </li>
                            <li class="link">
                                <a href="classes/IValue.html" data-type="entity-link">IValue</a>
                            </li>
                            <li class="link">
                                <a href="classes/IVar.html" data-type="entity-link">IVar</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/CanLogService.html" data-type="entity-link">CanLogService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CanRawService.html" data-type="entity-link">CanRawService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});