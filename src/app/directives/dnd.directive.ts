import { Directive, HostListener, HostBinding, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appDnd]'
})
export class DndDirective {

  @HostBinding('style.background') private background = '#eee';
  @Output() hovered = new EventEmitter<boolean>();
  @Output() tooMuchData = new EventEmitter<boolean>();
  @Output() fileDropped = new EventEmitter<FileList>();

  constructor() { }

  @HostListener('dragover', ['$event']) public onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#70838e';
    this.hovered.emit(true);
    const files = evt.dataTransfer.files;
    console.log('hovering data lelelel!');
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#eee';
    this.hovered.emit(false);

  }

  @HostListener('drop', ['$event']) public onDrop(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    const files = evt.dataTransfer.files;
    if (files.length > 0 && files.length <= 1) {
      console.log('transferstart lelelele!');
      this.background = '#eee';
      this.hovered.emit(false);
      console.log(files);
      this.tooMuchData.emit(false);
      this.fileDropped.emit(files);

    } else {
      this.tooMuchData.emit(true);
      this.background = '#eee';
      this.hovered.emit(false);
    }

  }


}
