export class IDocument {
    constructor(
        public name: string,
        public Version: number,
        public author: string,
        public comp: string,
        public doc_data: string
    ) {

    }
}