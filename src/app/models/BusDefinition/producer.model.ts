import { INodeRef } from './noderef.model';

export class IProducer {
    constructor(
        public prod_noderefs: Array<INodeRef>
    ) {

    }
}