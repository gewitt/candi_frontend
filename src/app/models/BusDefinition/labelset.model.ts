import { ILabel } from './label.model';
import { ILabelGroup } from './labelgroup.model';

export class ILabelSet {
    constructor(
        public labels: Array<ILabel>,
        public labelGroups: Array<ILabelGroup>
    ) {

    }
}