import { IVar } from './var.model';

export class INode {
    constructor(
        public node_id: string,
        public node_name: string,
        public node_vars: Array<IVar>
    ) {

    }
}