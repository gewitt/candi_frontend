export class INodeRef {
    constructor(
        public noderef_id: string,
        public noderef: string
    ) {

    }
}