import { IMuxGroup } from './muxgroup.model';

export class IMultiplex {
    constructor(
        public mux_name: string,
        public mux_offset: string,
        public mux_length: string,
        public mux_groups: Array<IMuxGroup>
    ) {

    }
}