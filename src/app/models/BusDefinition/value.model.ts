export class IValue {
    constructor(
        public val_type: string,
        public val_slope: number,
        public val_intercept: number,
        public val_unit: string,
        public val_min: number,
        public val_max: number,
        public val_value: number
    ) {

    }
}