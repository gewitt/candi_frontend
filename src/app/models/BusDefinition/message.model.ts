import { IProducer } from './producer.model';
import { IMultiplex } from './mux.model';
import { ISignal } from './signal.model';

export class IMessage {
    constructor(
        public msg_id: string,
        public msg_name: string,
        public msg_triggered: string,
        public msg_interval: number,
        public msg_count: string,
        public msg_format: string,
        public msg_notes: string,
        public msg_producer: IProducer,
        public msg_mux: IMultiplex,
        public msg_signals: Array<ISignal>
    ) {

    }
}