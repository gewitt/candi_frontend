export class ILabel {
    constructor(
        public lab_name: string,
        public lab_value: number,
        public lab_type: string,
        public lab_from: number,
        public lab_to: number
    ) {

    }
}