export class ILabelGroup {
    constructor(
        public labg_type: string,
        public labg_name: string,
        public labg_from: number,
        public labg_to: number,
        public labg_value: number
    ) {

    }
}