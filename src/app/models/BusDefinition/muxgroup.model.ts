import { ISignal } from './signal.model';

export class IMuxGroup {
    constructor(
        public muxg_count: string,
        public muxg_signals: Array<ISignal>
    ) {

    }
}