import { IMessage } from './message.model';

export class IBus {
    constructor(
        public bus_name: string,
        public bus_baud: number,
        public bus_msgs: Array<IMessage>
    ) {

    }
}