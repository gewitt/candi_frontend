import { IConsumer } from './consumer.model';

import { IValue } from './value.model';

import { ILabelSet } from './labelset.model';

export class ISignal {
    constructor(
        public _id: string,
        public _rev: string,
        public sig_id: string,
        public sig_offset: string,
        public sig_length: string,
        public sig_endian: string,
        public sig_notes: string,
        public sig_consumer: IConsumer,
        public sig_value: IValue,
        public sig_labelsets: Array<ILabelSet>
    ) {
    }
}
