import { INodeRef } from './noderef.model';

export class IConsumer {
    constructor(
        public cons_noderefs: Array<INodeRef>
    ) {

    }
}