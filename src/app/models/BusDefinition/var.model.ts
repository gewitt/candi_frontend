import { IValue } from './value.model';

export class IVar {
    constructor(
        public var_name: string,
        public var_notes: string,
        public var_value: IValue
    ) {

    }
}