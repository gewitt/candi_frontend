import { IDocument } from './document.model';

import { INode } from './node.model';

import { IBus } from './bus.model';

export class INetworkDefinition {
    constructor(
        public document: IDocument,
        public nodes: Array<INode>,
        public buses: Array<IBus>

    ) {

    }
}