export class CanData {
  constructor(
    public tStamp: Date,
    public sig_name: string,
    public idHex: string,
    public ext: boolean,
    public data: string,
    public dlc: number,
  ) { }
}
