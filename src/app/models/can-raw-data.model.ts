export class CanRaw {
  constructor(
    public id: number,
    public timestamp: Date,
    public canid: number,
    public ext: any,
    public data: string,
    public dlc: number,
    public name: string
  ) { }
}
