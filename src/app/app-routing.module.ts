import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessageLoopComponent } from './components/message-loop/message-loop.component';
import { UploadDefinitionComponent } from './components/upload-definition/upload-definition.component';
import { VisualCanComponent } from './components/visual-can/visual-can.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: 'canLoop',
    component: MessageLoopComponent
  },
  {
    path: '',
    redirectTo: 'canLoop',
    pathMatch: 'full'
  },

  {
    path: 'upload',
    component: UploadDefinitionComponent
  },
  {
    path: 'viz',
    component: VisualCanComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  },

];

export const routingModule: ModuleWithProviders = RouterModule.forRoot(routes);

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
