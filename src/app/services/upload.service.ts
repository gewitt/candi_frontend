import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  private uploadUrl = environment.Url + '/upload';

  constructor(private http: HttpClient, private router: Router) { }

  // upload file to server
  uploadFile(event: FileList): Observable<HttpEvent<any>> {
    console.log('dropped File');
    const file = event.item(0);
    const formData = new FormData();
    formData.append('canDefinition', file, file.name);
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'text/xml');
    return this.http.post<any>(this.uploadUrl, formData, {
      reportProgress: true,
      headers: headers,
      observe: 'events'
    });
  }
}
