import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private subject: WebSocketSubject<any>;

  constructor() {


  }

  public getSubject(endpoint: string) {
    this.subject = webSocket(`${environment.wsUrl}/` + endpoint);
    console.log(this.subject);
    return this.subject;
  }
  
  public closeSubject() {
    return this.subject.complete();

  }



}
