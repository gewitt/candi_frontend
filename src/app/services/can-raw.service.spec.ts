import { TestBed } from '@angular/core/testing';

import { CanRawService } from './can-raw.service';

describe('CanRawService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CanRawService = TestBed.get(CanRawService);
    expect(service).toBeTruthy();
  });
});
