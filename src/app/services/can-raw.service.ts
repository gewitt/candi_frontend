import { Injectable } from '@angular/core';
import { CanRaw } from '../models/can-raw-data.model';
import { CanData } from '../models/can-data.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CanRawService {
  private rawData: CanRaw[] = [];
  private canData: CanData[] = [];

  constructor(private http: HttpClient) { }
  addCanRaw(

    timestamp: Date,
    id: number,
    canid: number,
    ext: any,
    data: string,
    dlc: number,
    name: string
  ) {
    this.rawData.push(new CanRaw(id, timestamp, canid, ext, data, dlc, name));
    console.log('added rawData: ', this.rawData);
  }
  // get CAN-Messages from DB
  getRawData(): Observable<CanRaw[]> {
    return this.http.get<CanRaw[]>(environment.Url + '/api/msgs');
  }

  // convert can-raw to readable data
  addCanData(ts_sec: Date, sig_name: string, id: number, ext: boolean, data: string, dlc) {
    console.log('id: ',id);
    console.log('date: ' )
    let canHex = ('000' + id.toString(16)).substr(-3).toUpperCase();
    
    this.canData.push(new CanData(ts_sec, sig_name, canHex , ext, data, dlc));
    console.log('added canData: ', this.canData);
  }

  getCanData() {
    return this.canData.slice();
  }
}
