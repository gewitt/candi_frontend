import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadDefinitionComponent } from './upload-definition.component';

describe('UploadDefinitionComponent', () => {
  let component: UploadDefinitionComponent;
  let fixture: ComponentFixture<UploadDefinitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadDefinitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadDefinitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
