import { Component, OnInit } from '@angular/core';
import { UploadService } from '../../services/upload.service';

import { MatSnackBar } from '@angular/material';
import { HttpEventType } from '@angular/common/http';


@Component({
  selector: 'app-upload-definition',
  templateUrl: './upload-definition.component.html',
  styleUrls: ['./upload-definition.component.scss']
})
export class UploadDefinitionComponent implements OnInit {

  // if user is hovering over dropzone
  isHovering: boolean;

  // if user selected too much files to upload
  tooMuchData: boolean;

  // percentage used fo progressbar
  progressState: number;

  constructor(private snackBar: MatSnackBar, private uploadService: UploadService) { }

  ngOnInit() {
  }

  toggleHover(event: boolean) {
    this.isHovering = event;
    console.log(event);
  }

  dataValid(event: boolean) {
    this.tooMuchData = event;
    console.log(event);
  }

  startUpload(event: FileList) {
    this.uploadService.uploadFile(event)
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          console.log('Upload Progress: ' + Math.round(event.loaded / event.total * 100) + ' %');
          this.progressState = Math.round(event.loaded / event.total * 100);
        } else if (event.type === HttpEventType.Response) {
          console.log('Got response!');
          console.log('id: ', event);
          this.snackBar.open('Datei erfolgreich hochgeladen!', 'OK', { duration: 10000 });
        }
      });
  }

}
