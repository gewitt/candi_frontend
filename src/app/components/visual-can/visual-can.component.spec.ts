import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualCanComponent } from './visual-can.component';

describe('VisualCanComponent', () => {
  let component: VisualCanComponent;
  let fixture: ComponentFixture<VisualCanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualCanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualCanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
