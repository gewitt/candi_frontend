import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { WebsocketService } from 'src/app/services/websocket.service';
import { ISignal } from 'src/app/models/BusDefinition/signal.model';

@Component({
  selector: 'app-visual-can',
  templateUrl: './visual-can.component.html',
  styleUrls: ['./visual-can.component.scss']
})
export class VisualCanComponent implements OnInit, OnDestroy {
  signalQuery: string;
  signals: Array<ISignal>;

  gaugeType = 'arch';
  gaugeValue: number;
  gaugeLabel = 'Speed';
  gaugeAppendKmh = 'km/h';
  gaugeSize = 600;

  constructor(private wsSrv: WebsocketService) {

  }
  ngOnInit() {
    //connect Websocket and listen for incoming Messages
    this.wsSrv.getSubject('viz')
      .subscribe(
        res => {
          this.signals = res;
          this.signals.forEach(signal => {
            if (signal.sig_id === 'SpeedKm') {
              this.gaugeLabel = signal.sig_id;
              this.gaugeValue = Math.round(signal.sig_value.val_value * 10) / 10 ;
              this.gaugeAppendKmh = 'km/h';
              this.gaugeType = 'arch';
            }
          });
          console.log('signals', res);

        },
        err => console.log(err),
        () => console.log('complete'));
  }

  ngOnDestroy() {
    this.wsSrv.closeSubject();
  }



}
