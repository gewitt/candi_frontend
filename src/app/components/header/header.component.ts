import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  clickedLog;
  clickedUpload;
  clickedViz;

  constructor(private router: Router) { }

  ngOnInit() {}

  onCanLog() {
    console.log('Can-Log');
    //this.router.navigate(['/canLoop']);
    this.clickedLog = true;
    this.clickedUpload = false;
    this.clickedViz = false;
  }

  onUpload() {
    console.log('on upload');
    //this.router.navigate(['/upload']);
    this.clickedUpload = true;
    this.clickedLog = false;
    this.clickedViz = false;
  }

  onVisual() {
    console.log('on viz');
    //this.router.navigate(['/viz']);
    this.clickedUpload = false;
    this.clickedLog = false;
    this.clickedViz = true;
  }
}
