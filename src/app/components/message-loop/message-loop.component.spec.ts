import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageLoopComponent } from './message-loop.component';

describe('MessageLoopComponent', () => {
  let component: MessageLoopComponent;
  let fixture: ComponentFixture<MessageLoopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageLoopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageLoopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
