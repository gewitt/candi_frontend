import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

import { CanData } from '../../models/can-data.model';
import { CanRawService } from 'src/app/services/can-raw.service';
import { CanRaw } from 'src/app/models/can-raw-data.model';
import { WebsocketService } from 'src/app/services/websocket.service';

@Component({
  selector: 'app-message-loop',
  templateUrl: './message-loop.component.html',
  styleUrls: ['./message-loop.component.scss']
})
export class MessageLoopComponent implements OnInit, OnDestroy {
  public dataSource: MatTableDataSource<CanData>;
  public displayedColumns: string[] = ['time', 'canId', 'sig_name', 'dlc', 'data'];
  public canMsgs: Array<CanRaw>;
  public canMsg: CanRaw;
  public strSeq = '11 22 33 44 55 66 77 88';

  constructor(private rawSrv: CanRawService, private sockSrv: WebsocketService) {
    // Get messages from DB
    this.rawSrv.getRawData().subscribe(msgs => {
      this.canMsgs = msgs;
      if (msgs === null || msgs === undefined || this.rawSrv.getCanData()===undefined || this.rawSrv.getCanData()===null) {
        return;
      } else {
        
        this.canMsgs.forEach(msg => {
          this.canMsg = msg;
          if (this.rawSrv.getCanData().length!==this.canMsgs.length){
          this.rawSrv.addCanData(
            this.canMsg.timestamp,
            this.canMsg.name,
            this.canMsg.canid,
            this.canMsg.ext,
            this.canMsg.data,
            this.canMsg.dlc);}
        });
        //set datasource for mattable
        this.dataSource = new MatTableDataSource<CanData>(this.rawSrv.getCanData());
      }
    });
  }

  ngOnInit() {
    // connect to Websocket and listen for incoming messages
    this.sockSrv.getSubject('canLoop')
      .subscribe(
        msg => {

          this.canMsg = msg;
          this.rawSrv.addCanRaw(this.canMsg.timestamp, this.canMsg.id, this.canMsg.canid, this.canMsg.ext, this.canMsg.data, this.canMsg.dlc, this.canMsg.name);
          this.rawSrv.addCanData(this.canMsg.timestamp, this.canMsg.name, this.canMsg.canid, this.canMsg.ext, this.canMsg.data, this.canMsg.dlc);
          //this.canMsgs.push(msg);
          console.log('Message from CAN: ', msg);
          //console.log('canMsgs[]', this.canMsgs);
          this.dataSource = new MatTableDataSource<CanData>(this.rawSrv.getCanData());

        },
        err => console.log(err),
        () => console.log('complete')
      );
  }

  ngOnDestroy() {
    this.sockSrv.closeSubject();
  }


}
