import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatToolbarModule, MatTabsModule, MatButtonModule, MatTableModule, MatSortModule, MatInputModule,
  MatPaginatorModule, MatSnackBarModule, MatGridListModule, MatDialogModule, MatCardModule, MatIconModule, MatProgressBarModule
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MessageLoopComponent } from './components/message-loop/message-loop.component';
import { WebsocketService } from './services/websocket.service';
import { UploadDefinitionComponent } from './components/upload-definition/upload-definition.component';
import { DndDirective } from './directives/dnd.directive';
import { UploadService } from './services/upload.service';
import { HttpClientModule } from '@angular/common/http';
import { VisualCanComponent } from './components/visual-can/visual-can.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { NgxGaugeModule } from 'ngx-gauge';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MessageLoopComponent,
    UploadDefinitionComponent,
    DndDirective,
    VisualCanComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    MatToolbarModule,
    MatTabsModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule, MatInputModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatGridListModule,
    MatDialogModule,
    MatCardModule,
    MatIconModule,
    MatProgressBarModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxGaugeModule
  ],
  providers: [MessageLoopComponent, WebsocketService, UploadService],
  bootstrap: [AppComponent]
})
export class AppModule { }
